/*
* Sample API with POST endpoint.
* POST data is set into redis server.
* Sent to RabbitMQ after data is set to redis
 */
package main

import (
	"encoding/json"
	"flag"
	"github.com/gomodule/redigo/redis"
	"github.com/google/uuid"
	"github.com/streadway/amqp"
	"io/ioutil"
	"log"
	"net/http"
)

var (
	// flagPort is the open port the application listens on
	flagPort = flag.String("port", "9000", "Port to listen on")
)

// Task Structure for TaskHandler request body
type Task struct {
	Title string `json: "title"`
}

// TaskHandler puts post request body to redis
func TaskHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		newTask := Task{}
		// read body
		body, err := ioutil.ReadAll(r.Body)

		if err != nil {
			panic(err)
		}

		_ = json.Unmarshal([]byte(body), &newTask)

		// redis connection creation
		conn, err := redis.Dial("tcp", "localhost:6379")
		if err != nil {
			log.Fatal(err)
		}
		defer conn.Close()

		// generating a uuid as key for the message
		key := uuid.New().String()

		// Setting data in redis
		_, err = conn.Do("SET", key, newTask.Title)

		if err != nil {
			log.Fatal(err)
		}
		// connecting rabbitmq env
		connection, err := amqp.Dial("amqp://guest:guest@localhost:5672")
		if err != nil {
			panic("could not establish connection with RabbitMQ:" + err.Error())
		}
		defer connection.Close()

		channel, err := connection.Channel()
		if err != nil {
			panic("could not open RabbitMQ channel:" + err.Error())
		}
		err = channel.ExchangeDeclare("events", "topic", true, false, false, false, nil)
		if err != nil {
			panic(err)
		}

		message := amqp.Publishing{
			Body: []byte(key),
		}

		// sending message to queue
		err = channel.Publish("events", key, false, false, message)
		if err != nil {
			panic("error publishing a message to the queue:" + err.Error())
		}

		// return request setup
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte(`{"message": "Request Queued Successfully"}`))
	} else {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
	}
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/add-task", TaskHandler)
	log.Printf("listening on port %s", *flagPort)
	log.Fatal(http.ListenAndServe(":"+*flagPort, mux))
}
